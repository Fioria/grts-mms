package com.mms.entity;

/**
 * 枚举：客户状态
 * 
 * @author Administrator
 *
 */
public enum CustomerState {
	/**
	 * 意向不明
	 */
	UNKNOWN,
	/**
	 * 有意向
	 */
	INTENTIONAL,
	/**
	 * 已成交
	 */
	DEALED;
}
