package com.mms.entity;

import java.util.Date;

/**
 * 实体类：客户
 * 
 * @author Administrator
 *
 */
public class Customer {
	// 客户id，唯一
	private int custId;
	// 客户姓名
	private String name;
	// 客户姓别
	private Gender gender;
	// 客户联系地址
	private String address;
	// 客户联系电话
	private String phone;
	// 信息来源
	private InfoSource source;
	// 状态：意向不明，有意向，已成交
	private CustomerState state;
	// 最后一次跟进的日期
	private Date lastFollowUpDate;
	// 最后一次跟进的情况
	private String lastFollowUpNote;
	// 报备日期
	private Date reportToDate;
	// 成交日期
	private Date dealDate;
	// 该客户属于哪个用户
	private String ownerUsername;

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public InfoSource getSource() {
		return source;
	}

	public void setSource(InfoSource source) {
		this.source = source;
	}

	public CustomerState getState() {
		return state;
	}

	public void setState(CustomerState state) {
		this.state = state;
	}

	public Date getLastFollowUpDate() {
		return lastFollowUpDate;
	}

	public void setLastFollowUpDate(Date lastFollowUpDate) {
		this.lastFollowUpDate = lastFollowUpDate;
	}

	public String getLastFollowUpNote() {
		return lastFollowUpNote;
	}

	public void setLastFollowUpNote(String lastFollowUpNote) {
		this.lastFollowUpNote = lastFollowUpNote;
	}

	public Date getReportToDate() {
		return reportToDate;
	}

	public void setReportToDate(Date reportToDate) {
		this.reportToDate = reportToDate;
	}

	public Date getDealDate() {
		return dealDate;
	}

	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}

	public String getOwnerUsername() {
		return ownerUsername;
	}

	public void setOwnerUsername(String ownerUsername) {
		this.ownerUsername = ownerUsername;
	}

	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", name=" + name + ", gender=" + gender + ", address=" + address
				+ ", phone=" + phone + ", source=" + source + ", state=" + state + ", lastFollowUpDate="
				+ lastFollowUpDate + ", lastFollowUpNote=" + lastFollowUpNote + ", reportToDate=" + reportToDate
				+ ", dealDate=" + dealDate + ", ownerUsername=" + ownerUsername + "]";
	}

}
