package com.mms.entity;

import java.util.Date;

/**
 * 实体类：跟进
 * 
 * @author Administrator
 *
 */
public class FollowUp {
	// 跟进记录的id，唯一
	private int followUpId;
	// 跟进日期
	private Date followUpDate;
	// 跟进人
	private String followUpUsername;
	// 跟进的是哪一个客户
	private int custId;
	// 跟进情况
	private String followUpNote;
	// 经过本次跟进，客户是什么状态
	private CustomerState customerState;

	public int getFollowUpId() {
		return followUpId;
	}

	public void setFollowUpId(int followUpId) {
		this.followUpId = followUpId;
	}

	public Date getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}

	public String getFollowUpUsername() {
		return followUpUsername;
	}

	public void setFollowUpUsername(String followUpUsername) {
		this.followUpUsername = followUpUsername;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getFollowUpNote() {
		return followUpNote;
	}

	public void setFollowUpNote(String followUpNote) {
		this.followUpNote = followUpNote;
	}

	public CustomerState getCustomerState() {
		return customerState;
	}

	public void setCustomerState(CustomerState customerState) {
		this.customerState = customerState;
	}

}
