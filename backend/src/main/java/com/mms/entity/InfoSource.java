package com.mms.entity;

/**
 * 枚举：客户信息来源
 * 
 * @author Administrator
 *
 */
public enum InfoSource {
	/**
	 * 口碑（转介绍）
	 */
	WORD_OF_MOUTH,
	/**
	 * 新媒体
	 */
	MEDIA,
	/**
	 * 地推活动
	 */
	PROMOTION
}
