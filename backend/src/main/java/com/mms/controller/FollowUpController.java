package com.mms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mms.entity.FollowUp;
import com.mms.service.FollowUpService;

@CrossOrigin
@RestController
public class FollowUpController {
	
	@Autowired
	private FollowUpService followUpService;

	@GetMapping("/followup/{custId}")
	public List<FollowUp> history(@PathVariable int custId) {
		return this.followUpService.listFollowUpHistory(custId);
	}
	
	@PostMapping("/followup") 
	public void save(@RequestBody FollowUp followUp) {
		this.followUpService.save(followUp);
	}
}
