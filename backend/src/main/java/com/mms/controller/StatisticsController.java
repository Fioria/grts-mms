package com.mms.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mms.service.StatisticsService;

@CrossOrigin
@RestController
public class StatisticsController {

	@Autowired
	private StatisticsService service;

	@GetMapping("/funnel")
	public List<Map<String, Object>> funnel(String username, @DateTimeFormat(pattern = "yyyy-MM-dd") Date begin,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date end) {
		return this.service.salesFunnel(username, begin, end);
	}
	
	@GetMapping("/conversionRate")
	public Map<String, String> conversionRate(String username, @DateTimeFormat(pattern = "yyyy-MM-dd") Date begin,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date end) {
		return this.service.conversionRate(username, begin, end);
	}
	
	@GetMapping("/dealTrend")
	public List<Map<String, Object>> dealTrend(String username, @DateTimeFormat(pattern = "yyyy-MM-dd") Date begin,
			@DateTimeFormat(pattern = "yyyy-MM-dd") Date end) {
		return this.service.dealTrend(username, begin, end);
	}
}
