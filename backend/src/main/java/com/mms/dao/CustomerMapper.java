package com.mms.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.mms.entity.Customer;
import com.mms.entity.CustomerState;


@Mapper
public interface CustomerMapper {

	/**
	 * 以主键作为条件，更新一个客户
	 * 
	 * @param customer
	 */
	void updateByPrimaryKey(Customer customer);

	/**
	 * 以主键作为条件，查询一个客户
	 * 
	 * @param custId
	 * @return
	 */
	Customer selectByPrimaryKey(int custId);

	/**
	 * 以姓名或电话为条件，进行模糊查询
	 * 
	 * @param nameOrPhone
	 * @return
	 */
	List<Customer> selectByNameOrPhoneLike(String nameOrPhone);

	/**
	 * 插入新客户
	 * 
	 * @param newCustomer
	 */
	void insert(Customer newCustomer);

	/**
	 * 根据主键删除客户
	 * 
	 * @param custId
	 */
	void deleteByPrimaryKey(int custId);

	long countByNameOrPhoneLikeAndStateAndOwnerUsername(
			@Param("nameOrPhone") String nameOrPhone, 
			@Param("customerState") CustomerState customerState,
			@Param("username") String username);

	List<Customer> selectByNameOrPhoneLikeAndStateAndOwnerUsernamePageable(
			@Param("nameOrPhone") String nameOrPhone,
			@Param("customerState") CustomerState customerState, 
			@Param("username") String username, 
			@Param("skip") int skip, 
			@Param("take") int take);

	@Update("UPDATE customer SET owner_username = #{newOwnerUsername} WHERE cust_id = #{custId}")
	void updateOwnerUsername(int custId, String newOwnerUsername);

}
