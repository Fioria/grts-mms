package com.mms.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.mms.entity.FollowUp;

@Mapper
public interface FollowUpMapper {
	
	/**
	 * 插入跟进记录
	 * @param newFollowUp
	 */
	void insert(FollowUp newFollowUp);
	
	/**
	 * 根据客户id查询跟进记录，按跟进id降序排序
	 * @param custId
	 * @return
	 */
	List<FollowUp> selectByCustId(int custId);
}
