package com.mms.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface StatisticsMapper {

	List<Map<String, Object>> countByUsernameAndReportToDateGroupByState(
			@Param("usernames") List<String> usernames, 
			@Param("begin") Date begin, 
			@Param("end") Date end);

	List<Map<String, Object>> countByUsernameAndDealDateGroupByYearMonth(
			@Param("usernames") List<String> usernames, 
			@Param("begin") Date begin, 
			@Param("end") Date end);

}
