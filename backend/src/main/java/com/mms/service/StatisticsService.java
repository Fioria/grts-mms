package com.mms.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 统计模块的业务接口
 * 
 * @author Administrator
 *
 */
public interface StatisticsService {

	/**
	 * 销售漏斗数据统计
	 * 
	 * @param username 用户帐号。如果该帐号角色为NORMAL，则统计他单人的数据；如果该帐号角色为MANAGER，则统计整个团队的数据
	 * @param begin    起始日期
	 * @param end      终止日期
	 * @return 返回统计结果，如：
	 [
	 {count=53, state=DEALED}
	 {count=57, state=INTENTIONAL}
	 {count=59, state=UNKNOWN}
	 ]
	 */
	List<Map<String, Object>> salesFunnel(String username, Date begin, Date end);

	/**
	 * 销售转化率统计
	 * 
	 * @param username 用户帐号。如果该帐号角色为NORMAL，则统计他单人的数据；如果该帐号角色为MANAGER，则统计整个团队的数据
	 * @param begin    起始日期
	 * @param end      终止日期
	 * @return 返回统计结果，如：{ "报备客户数": "100", "意向客户数": "50", "成交客户数": "10", "意向转化率":
	 *         "50%", "成交转化率": "25%" }
	 */
	Map<String, String> conversionRate(String username, Date begin, Date end);

	/**
	 * 成交趋势统计
	 * 
	 * @param username 用户帐号。如果该帐号角色为NORMAL，则统计他单人的数据；如果该帐号角色为MANAGER，则统计整个团队的数据
	 * @param begin    起始日期
	 * @param end      终止日期
	 * @return 返回统计结果，如：
	 */
	List<Map<String, Object>> dealTrend(String username, Date begin, Date end);
}
