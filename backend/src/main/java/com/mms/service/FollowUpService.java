package com.mms.service;

import java.util.List;

import com.mms.entity.FollowUp;

/**
 * 跟进模块的业务接口
 * @author Administrator
 *
 */
public interface FollowUpService {

	/**
	 * 保存一次新的跟进记录
	 * @param followUp
	 */
	void save(FollowUp newFollowUp);
	
	/**
	 * 查询指定客户的历史跟进记录，按时间降序排序
	 * @param custId 客户的唯一id
	 * @return 返回查询结果；如果一次跟进都没有，返回null
	 */
	List<FollowUp> listFollowUpHistory(int custId);
}
